# Document reader

we can extract the data present in the document and convert it into the text with the help of Tesseract/opencv.

## Tesseract

PyTesseract and the Tesseract Engine has many flaws when it comes to converting image to text, especially if 
the image is noisy and/or contains salt and pepper noise. This can be overcome later by implementing image 
classification algorithms using LSTMs for better accuracy.

## IDE and list of libraries used:

----------------------------------

1. PyCharm Community Edition running Python 3.6
2. Pillow 
3. [pytesseract](https://opensource.google.com/projects/tesseract)
4. cv2
5. re
6. json``
7. [ftfy](https://ftfy.readthedocs.io/en/latest/)
8. os
9. argparse
10. [nostril](https://www.theoj.org/joss-papers/joss.00596/10.21105.joss.00596.pdf)

## Steps to run the task

1. Run the app.py file.
2. Select the type of document that you are going to Upload.
3. Upload the document that you want to convert into text format.
4. Then the document and the converted text will be displayed.

## Working

1. First image is read from the front end(ie.HTML).
2. Then the image should be converted into grayscale.
3. For more accuracy of text we convert the image into adaptive.
4. Then Cleaning all the gibberish text.
5. After Cleaning we can extract the relevant data.
