from __future__ import print_function
from flask import Flask, render_template, request
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
import cv2
import pytesseract
from PIL import Image
import pytesseract
import argparse
import cv2
import os
import re
import io
import ftfy
import nonsense
import urllib.request


import eel

app = Flask(__name__)
eel.init('templates')

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'static'

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/', methods=['GET', 'POST'])
def getvalue():
    myvariable = request.form.get("identity")
    print(myvariable)
    if request.method == 'POST':
        f=request.files['file']
        fn = f.filename
        print(f.filename)
        print('file uploaded')

    if myvariable == "aadhar":
        pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
        full_filename = os.path.join(app.config['UPLOAD_FOLDER'], fn)
        image = cv2.imread(fn)
        grayscale = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

        adaptive = cv2.adaptiveThreshold(grayscale, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 11)

        text = pytesseract.image_to_string(image)
        text = pytesseract.image_to_string(grayscale)
        text = pytesseract.image_to_string(adaptive)

        # cv2.imshow("image",image)
        # cv2.imshow("gray",grayscale)
        # cv2.imshow("adaptive", adaptive)
        cv2.waitKey(0)

        text_output = open('outputbase.txt', 'w', encoding='utf-8')
        text_output.write(text)
        text_output.close()

        file = open('outputbase.txt', 'r', encoding='utf-8')
        text = file.read()
        # print(text)

        # Cleaning all the gibberish text
        text = ftfy.fix_text(text)
        text = ftfy.fix_encoding(text)

        '''for god_damn in text:
            if nonsense(god_damn):
                text.remove(god_damn)
            else:
                print(text)'''

        # Initializing data variable
        name = None
        yob = None
        gender = None
        adhar = None
        nameline = []
        dobline = []
        panline = []
        text0 = []
        text1 = []
        text2 = []

        # Searching for PAN
        lines = text.split('\n')
        for lin in lines:
            s = lin.strip()
            s = lin.replace('\n', '')
            s = s.rstrip()
            s = s.lstrip()
            text1.append(s)

        text1 = list(filter(None, text1))
        print(text1)
        # print(text)

        lineno = 0
        for wordline in text1:
            xx = wordline.split('\n')
            if ([w for w in xx if re.search(
                    '(INCOMETAXDEPARWENT @|mcommx|INCOME|TAX|GOW|GOVT|GOVERNMENT|OVERNMENT|VERNMENT|DEPARTMENT|EPARTMENT|PARTMENT|ARTMENT|INDIA|NDIA)$',
                    w)]):
                text1 = list(text1)
                lineno = text1.index(wordline)
                break

        print(lineno)

        # text1 = list(text1)
        text0 = text1[lineno + 1:]
        print(text0)  # Contains all the relevant extracted text in form of a list - uncomment to check

        def findword(textlist, wordstring):
            lineno = -1
            for wordline in textlist:
                xx = wordline.split()
                if ([w for w in xx if re.search(wordstring, w)]):
                    lineno = textlist.index(wordline)
                    textlist = textlist[lineno + 1:]
                    return textlist
            return textlist

        try:

            # Cleaning Name
            name = text0[2]
            name = name.rstrip()
            name = name.lstrip()
            name = re.sub('[^a-zA-Z] +', ' ', name)

            # Cleaning YOB
            yob = text0[3]
            yob = re.sub('[^0-9]+', '', yob)
            yob = yob.replace(" ", "")
            yob = yob[0:6]
            yob = yob.rstrip()
            yob = yob.lstrip()

            # Cleaning Gender
            gender = text0[4]
            gender = gender.replace('/', '')
            gender = gender.replace('(', '')
            gender = gender[6:12]
            gender = gender.rstrip()
            gender = gender.lstrip()

            # Cleaning Aadhar Number
            adhar = text0[6]
            adhar = re.sub('[^0-9]+', '', adhar)
            adhar = adhar.rstrip()
            adhar = adhar.lstrip()

        except:
            pass

        # Making tuples of data
        data = {}
        data['Name'] = name
        data['Year of Birth'] = yob
        data['Gender'] = gender
        data['Number'] = adhar
        print(full_filename)
        print(data)
        return render_template("document.html", pic=full_filename, name=name, yob=yob, gender=gender, adhar=adhar)

    if myvariable == "pan":
        pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
        pic = "pancard-sample_cropped.jpg"
        full_filename = os.path.join(app.config['UPLOAD_FOLDER'], fn)
        image = cv2.imread(fn)
        grayscale = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

        adaptive = cv2.adaptiveThreshold(grayscale, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 11)

        # text = pytesseract.image_to_string(image)
        # text = pytesseract.image_to_string(grayscale)
        text = pytesseract.image_to_string(adaptive)

        # cv2.imshow("image",image)
        # cv2.imshow("gray",grayscale)
        # cv2.imshow("adaptive", adaptive)
        cv2.waitKey(0)

        # writing extracted data into a text file
        text_output = open('outputbase.txt', 'w', encoding='utf-8')
        text_output.write(text)
        text_output.close()

        file = open('outputbase.txt', 'r', encoding='utf-8')
        text = file.read()
        # print(text)

        # Cleaning all the gibberish text
        text = ftfy.fix_text(text)
        text = ftfy.fix_encoding(text)
        '''for god_damn in text:
            if nonsense(god_damn):
                text.remove(god_damn)
            else:
                print(text)'''
        # print(text)

        # Initializing data variable
        name = None
        fname = None
        dob = None
        pan = None
        nameline = []
        dobline = []
        panline = []
        text0 = []
        text1 = []
        text2 = []

        # Searching for PAN
        lines = text.split('\n')
        for lin in lines:
            s = lin.strip()
            s = lin.replace('\n', '')
            s = s.rstrip()
            s = s.lstrip()
            text1.append(s)

        text1 = list(filter(None, text1))
        # print(text1)

        lineno = 0  # to start from the first line of the text file.

        for wordline in text1:
            xx = wordline.split('\n')
            if ([w for w in xx if re.search(
                    '(INCOMETAXDEPARWENT @|mcommx|INCOME|TAX|GOW|GOVT|GOVERNMENT|OVERNMENT|VERNMENT|DEPARTMENT|EPARTMENT|PARTMENT|ARTMENT|INDIA|NDIA)$',
                    w)]):
                text1 = list(text1)
                lineno = text1.index(wordline)
                break

        # text1 = list(text1)
        text0 = text1[lineno + 1:]
        print(text0)  # Contains all the relevant extracted text in form of a list - uncomment to check

        def findword(textlist, wordstring):
            lineno = -1
            for wordline in textlist:
                xx = wordline.split()
                if ([w for w in xx if re.search(wordstring, w)]):
                    lineno = textlist.index(wordline)
                    textlist = textlist[lineno + 1:]
                    return textlist
            return textlist

        try:

            # Cleaning first names, better accuracy
            name = text0[0]
            name = name.rstrip()
            name = name.lstrip()
            name = name.replace("8", "B")
            name = name.replace("0", "D")
            name = name.replace("6", "G")
            name = name.replace("1", "I")
            name = re.sub('[^a-zA-Z] +', ' ', name)

            # Cleaning Father's name
            fname = text0[2]
            fname = fname.rstrip()
            fname = fname.lstrip()
            fname = fname.replace("8", "S")
            fname = fname.replace("0", "O")
            fname = fname.replace("6", "G")
            fname = fname.replace("1", "I")
            fname = fname.replace("\"", "A")
            fname = re.sub('[^a-zA-Z] +', ' ', fname)

            # Cleaning DOB
            dob = text0[3]
            dob = dob.rstrip()
            dob = dob.lstrip()
            dob = dob.replace('l', '/')
            dob = dob.replace('L', '/')
            dob = dob.replace('I', '/')
            dob = dob.replace('i', '/')
            dob = dob.replace('|', '/')
            dob = dob.replace('\"', '/1')
            dob = dob.replace(" ", "")

            # Cleaning PAN Card details
            text0 = findword(text1, '(Pormanam|Number|umber|Account|ccount|count|Permanent|ermanent|manent|wumm)$')
            panline = text0[0]
            pan = panline.rstrip()
            pan = pan.lstrip()
            pan = pan.replace(" ", "")
            pan = pan.replace("\"", "")
            pan = pan.replace(";", "")
            pan = pan.replace("%", "L")

        except:
            pass

        # Making tuples of data
        data = {}
        data['Name'] = name
        data['Father Name'] = fname
        data['Date of Birth'] = dob
        data['PAN'] = pan

        print(data)
        return render_template("pan.html", pic=full_filename, name=name, fname=fname, dob=dob, pan=pan)

    if myvariable == "passport":
        pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
        pic = "passport-sample_cropped.jpg"
        full_filename = os.path.join(app.config['UPLOAD_FOLDER'], fn)
        image = cv2.imread(fn)
        grayscale = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

        adaptive = cv2.adaptiveThreshold(grayscale, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 11)

        # text = pytesseract.image_to_string(image)
        # text = pytesseract.image_to_string(grayscale)
        text = pytesseract.image_to_string(adaptive)

        # cv2.imshow("image",image)
        # cv2.imshow("gray",grayscale)
        # cv2.imshow("adaptive", adaptive)
        cv2.waitKey(0)

        # writing extracted data into a text file
        text_output = open('outputbase.txt', 'w', encoding='utf-8')
        text_output.write(text)
        text_output.close()

        #file = open('outputbase.txt', 'r', encoding='utf-8')
        #text = file.read()
        # print(text)

        # Cleaning all the gibberish text
        text = ftfy.fix_text(text)
        text = ftfy.fix_encoding(text)
        '''for god_damn in text:
            if nonsense(god_damn):
                text.remove(god_damn)
            else:
                print(text)'''
        # print(text)

        # Initializing data variable
        surname = None
        first_name = None
        dob = None
        gender = None
        number = None
        address = None
        doe = None
        text0 = []
        text1 = []

        # Searching for PAN
        lines = text.split('\n')
        for lin in lines:
            s = lin.strip()
            s = lin.replace('\n', '')
            s = s.rstrip()
            s = s.lstrip()
            text1.append(s)

        text1 = list(filter(None, text1))
        # print(text1)

        # to remove any text read from the image file which lies before the line 'Income Tax Department'

        lineno = 0  # to start from the first line of the text file.

        # text1 = list(text1)
        text0 = text1[lineno + 1:]
        print(text0)  # Contains all the relevant extracted text in form of a list - uncomment to check

        def findword(textlist, wordstring):
            lineno = -1
            for wordline in textlist:
                xx = wordline.split()
                if ([w for w in xx if re.search(wordstring, w)]):
                    lineno = textlist.index(wordline)
                    textlist = textlist[lineno + 1:]
                    return textlist
            return textlist

        try:

            # Cleaning Surname
            surname = text0[4]
            surname = surname.rstrip()
            surname = surname.lstrip()
            surname = re.sub('[^a-zA-Z] +', ' ', surname)

            # Cleaning First Name
            first_name = text0[5]
            first_name = first_name.rstrip()
            first_name = first_name.lstrip()
            first_name = re.sub('[^a-zA-Z] +', ' ', first_name)

            # Cleaning DOB
            dob = text0[7]
            dob = dob.rstrip()
            dob = dob.lstrip()
            dob = dob[-12:-1]

            # Cleaning Gender
            gender = text0[4]
            gender = 'M'  # need to fix this

            # Cleaning Passport Number
            number = text0[2]
            number = number[-9:]
            number = number.rstrip()
            number = number.lstrip()

            address = text0[10]
            address = address[-18:]

            # Cleaning DOE
            doe = text0[15]
            doe = doe.rstrip()
            doe = doe.lstrip()
            doe = doe[-10:]

        except:
            pass

        # Making tuples of data
        data = {}
        data['Surname'] = surname
        data['First Name'] = first_name
        data['Date of Birth'] = dob
        data['Gender'] = gender
        data['Number'] = number
        data['Address'] = address
        data['Date of Expiry'] = doe

        print(data)
        return render_template("passport.html", pic=full_filename, surname=surname, first_name=first_name, dob=dob, gender=gender, number=number, address=address, doe=doe)



    if myvariable == "driving":
        pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
        pic = "driving_licence_sample_cropped.jpg"
        full_filename = os.path.join(app.config['UPLOAD_FOLDER'], fn)
        image = cv2.imread(fn)
        grayscale = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

        adaptive = cv2.adaptiveThreshold(grayscale, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 11)

        # text = pytesseract.image_to_string(image)
        text = pytesseract.image_to_string(grayscale)
        # text = pytesseract.image_to_string(adaptive)

        # cv2.imshow("image",image)
        # cv2.imshow("gray",grayscale)
        # cv2.imshow("adaptive", adaptive)
        cv2.waitKey(0)

        # writing extracted data into a text file
        text_output = open('outputbase.txt', 'w', encoding='utf-8')
        text_output.write(text)
        text_output.close()

        file = open('outputbase.txt', 'r', encoding='utf-8')
        text = file.read()
        # print(text)

        # Cleaning all the gibberish text
        text = ftfy.fix_text(text)
        text = ftfy.fix_encoding(text)
        '''for god_damn in text:
            if nonsense(god_damn):
                text.remove(god_damn)
            else:
                print(text)'''
        print(text)

        # Initializing data variable
        surname = None
        dob = None
        doe = None
        number = None
        text0 = []
        text1 = []

        # Searching for PAN
        lines = text.split('\n')
        for lin in lines:
            s = lin.strip()
            s = lin.replace('\n', '')
            s = s.rstrip()
            s = s.lstrip()
            text1.append(s)

        text1 = list(filter(None, text1))
        # print(text1)

        # to remove any text read from the image file which lies before the line 'Income Tax Department'

        lineno = 0  # to start from the first line of the text file.

        # text1 = list(text1)
        text0 = text1[lineno + 1:]
        print(text0)  # Contains all the relevant extracted text in form of a list - uncomment to check

        '''def findword(textlist, wordstring):
            lineno = -1
            for wordline in textlist:
                xx = wordline.split( )
                if ([w for w in xx if re.search(wordstring, w)]):
                    lineno = textlist.index(wordline)
                    textlist = textlist[lineno+1:]
                    return textlist
            return textlist'''

        try:

            # Cleaning name
            name = text0[7]
            name = re.sub('[^a-zA-Z]+', ' ', name)
            name = name[-10:]
            name = name.rstrip()
            name = name.lstrip()

            # Cleaning DOB
            dob = text0[6]
            dob = dob.rstrip()
            dob = dob.lstrip()
            dob = dob[-4:]

            # Cleaning DOE
            doe = text0[5]
            doe = doe.rstrip()
            doe = doe.lstrip()
            doe = doe[-12:]

        except:
            pass

        # Making tuples of data
        data = {}
        data['Name'] = name
        data['Date of Birth'] = dob
        data['Date of Issued'] = doe

        print(data)
        return render_template("driving.html", pic=full_filename, name=name, dob=dob, doe=doe)


if __name__ == "__main__":
    app.run()
